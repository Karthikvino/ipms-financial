module.exports = {
  name: 'ar',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/ar',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
};
