module.exports = {
  name: 'cm',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/cm',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
};
