import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
//import { TableModule } from 'primeng/table';

//import { IpmsNgTableModule } from 'libs/ipms-ng-table/src/lib/ipms-ng-table.component';

import { IpmsNgTableModule } from '@ipms-fsd/ipms-ng-table';
import { CarService } from './app.service';


@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, HttpClientModule, IpmsNgTableModule],
  providers: [CarService],
  bootstrap: [AppComponent],
})
export class AppModule {}
