module.exports = {
  name: 'gl',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/gl',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
};
