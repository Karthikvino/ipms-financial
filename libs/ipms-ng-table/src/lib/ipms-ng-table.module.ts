import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Table, TableBody, SortableColumn, SelectableRow,  RowToggler,  ContextMenuRow,  ResizableColumn,  ReorderableColumn,  EditableColumn,  CellEditor,  SortIcon,  TableRadioButton,  TableCheckbox,  TableHeaderCheckbox,  ReorderableRowHandle,  ReorderableRow,  SelectableRowDblClick,  EditableRow,  InitEditableRow,  SaveEditableRow,  CancelEditableRow, ScrollableView } from './ipms-ng-table.component';
import { SharedModule } from 'primeng/api';
import { PaginatorModule } from 'primeng/paginator';
import { ScrollingModule } from '@angular/cdk/scrolling';


@NgModule({
  imports: [
    CommonModule,
    PaginatorModule,
    ScrollingModule
  ],
  exports: [
      Table,
      SharedModule,
      SortableColumn,
      SelectableRow,
      RowToggler,
      ContextMenuRow,
      ResizableColumn,
      ReorderableColumn,
      EditableColumn,
      CellEditor,
      SortIcon,
      TableRadioButton,
      TableCheckbox,
      TableHeaderCheckbox,
      ReorderableRowHandle,
      ReorderableRow,
      SelectableRowDblClick,
      EditableRow,
      InitEditableRow,
      SaveEditableRow,
      CancelEditableRow,
      ScrollingModule
  ],
  declarations: [
      Table,
      SortableColumn,
      SelectableRow,
      RowToggler,
      ContextMenuRow,
      ResizableColumn,
      ReorderableColumn,
      EditableColumn,
      CellEditor,
      TableBody,
      ScrollableView,
      SortIcon,
      TableRadioButton,
      TableCheckbox,
      TableHeaderCheckbox,
      ReorderableRowHandle,
      ReorderableRow,
      SelectableRowDblClick,
      EditableRow,
      InitEditableRow,
      SaveEditableRow,
      CancelEditableRow
  ]
})
export class IpmsNgTableModule { }
